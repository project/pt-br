# pt-br translation of Drupal (modules/system/system.install)
# Copyright 2007 José San Martin <jose.sanmartin@verinco.com> (v5.x-2.0), Bruno Massa Guimarães <bruno.massa@titanatlas.com> (v5.x-1.0)
# Sponsor: Fabiano Sant'Ana <wundo@wundo.net> (v5.x-2.0)
# Generated from file: system.install,v 1.60 2006/12/13 10:41:56 unconed
#
msgid ""
msgstr ""
"Project-Id-Version: system-install\n"
"POT-Creation-Date: 2006-12-15 11:16+0100\n"
"PO-Revision-Date: 2007-06-11 23:38-0300\n"
"Last-Translator: José San Martin <jz.sanmartin@gmail.com>\n"
"Language-Team: Português do Brasil\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=2; plural=(n > 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: modules/system/system.install:104
msgid "You can <a href=\"@cron\">run cron manually</a>."
msgstr "Você rodar o <a href=\"@cron\"> agendador de tarefas</a> manualmente."

#: modules/system/system.install:3247
msgid "If you want to add a static page, like a contact page or an about page, use a page."
msgstr "Se você deseja adicionar uma página estática, como uma página de contato ou uma página sobre o site, use a página."

#: modules/system/system.install:3254
msgid "Story"
msgstr "Matéria"

#: modules/system/system.install:3256
msgid "Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles."
msgstr "Matérias são artigos em sua forma simples: possuem um título, uma chamada e um corpo, mas podem ser extendidas por outros módulos. A chamada é parte do corpo da matéria. Matérias podem ser usadas como um blog pessoal ou numa página de notícias."

#: modules/system/system.install:3356
msgid "URL Filter module was disabled; this functionality has now been added to core."
msgstr "Filtro de URL desbilitado; essa ferramenta agora é parte do núcleo do Drupal."

#: modules/system/system.install:30
msgid "Web server"
msgstr "Servidor web"

#: modules/system/system.install:39
msgid "Your Apache server is too old. Drupal requires at least Apache %version."
msgstr "A sua versão do servidor Apache. Para instalar o Drupal, é necessária a versão %version ou superior."

#: modules/system/system.install:45
msgid "The web server you're using has not been tested with Drupal and might not work properly."
msgstr "O servidor web que você está usando não foi testado com Drupal e pode haver falhas no funcionamento."

#: modules/system/system.install:51
msgid "Unknown"
msgstr "Desconhecido"

#: modules/system/system.install:52
msgid "Unable to determine your web server type and version. Drupal might not work properly."
msgstr "Não foi possível determinar a versão e o tipo do seu servidor web. Pode haver falhas no funcionamento do Drupal."

#: modules/system/system.install:62
msgid "Your PHP installation is too old. Drupal requires at least PHP %version."
msgstr "A sua instalação de PHP está desatualizada. Para instalar o Drupal, é necessária a versão %version ou superior."

#: modules/system/system.install:76
msgid "Not protected"
msgstr "Não protegido"

#: modules/system/system.install:78
msgid "The file %file is not protected from modifications and poses a security risk. You must change the file's permissions to be non-writable."
msgstr "O arquivo %file não está protegido contra escrita e isso é um risco à segurança do site. Você precisa alterar as permissões do arquivo para que ele se torne somente leitura."

#: modules/system/system.install:83
msgid "Protected"
msgstr "Protegido"

#: modules/system/system.install:86
msgid "Configuration file"
msgstr "Arquivo de configurações"

#: modules/system/system.install:94
msgid "Last run !time ago"
msgstr "Rodado há !time"

#: modules/system/system.install:98
msgid "Cron has not run. It appears cron jobs have not been setup on your system. Please check the help pages for <a href=\"@url\">configuring cron jobs</a>."
msgstr "O agendador de tarefas cron não rodou. Por favor, confira a ajuda on-line sobre <a href=\"@url\">configurações do agendador</a>."

#: modules/system/system.install:100
msgid "Never run"
msgstr "Nunca rodou"

#: modules/system/system.install:106
msgid "Cron maintenance tasks"
msgstr "Serviços de manutenção do Cron"

#: modules/system/system.install:119
msgid "The directory %directory is not writable."
msgstr "Sem permissões de escrita para o diretório %directory."

#: modules/system/system.install:122
msgid "Not writable"
msgstr "Sem permissões de escrita"

#: modules/system/system.install:124
msgid "You may need to set the correct directory at the <a href=\"@admin-file-system\">file system settings page</a> or change the current directory's permissions so that it is writable."
msgstr "É possível que você precise configurar o diretório correto na <a href=\"@admin-file-system\">página de configurações do sistema de arquivos</a> ou alterar as permissões do diretório atual para que haja permissões de escrita."

#: modules/system/system.install:130
msgid "Writable (<em>public</em> download method)"
msgstr "Com permissões de escrita (método de download <em>público</em>)"

#: modules/system/system.install:135
msgid "Writable (<em>private</em> download method)"
msgstr "Com permissões de escrita (método de download <em>privado</em>)"

#: modules/system/system.install:145
msgid "Database schema"
msgstr "Esquema de Banco de Dados"

#: modules/system/system.install:147
msgid "Up to date"
msgstr "Atualizado"

#: modules/system/system.install:157
msgid "Out of date"
msgstr "Desatualizado"

#: modules/system/system.install:158
msgid "Some modules have database schema updates to install. You should run the <a href=\"@update\">database update script</a> immediately."
msgstr "Alguns módulos tem que alterar suas tabelas do banco de dados. Você deve executar o <a href=\"@update\">script de atualização de banco de dados</a> imediatamente."

